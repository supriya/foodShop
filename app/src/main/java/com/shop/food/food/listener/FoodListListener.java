package com.shop.food.food.listener;

/**
 * Created by Supriya A on 2/2/2018.
 */

public interface FoodListListener {


    void onProceedToCheckoutClick();

    void onResetSuccess();

}
