package com.shop.food.app;

/**
 * Created by Supriya A on 1/2/2018.
 */
public class Constants {

    //relam constants
    public static final String REALM_TYPE_HACKER_NEWS = "hackernews.realm";
    public static final String KEY_HOTEL_NAME = "key_hotel_name";

    public static long LOCATION_INTERVAL = 1000;
    public static long FASTEST_INTERVAL = 1000;
    public static int PROXIMITY_RADIUS = 10000;

    public static String SEARCH_KEY_RESTAURANT = "restaurant";
}
