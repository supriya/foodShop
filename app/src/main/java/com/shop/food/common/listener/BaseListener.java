package com.shop.food.common.listener;


/**
 * Created by Supriya A on 2/2/2018.
 */

public interface BaseListener {
    void showProgressDialog(String message);
    void showProgressDialog();
    void removeProgressDialog();
}
